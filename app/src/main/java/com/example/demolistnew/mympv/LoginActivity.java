package com.example.demolistnew.mympv;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.demolistnew.R;
import com.example.demolistnew.entity.UserBean;
import com.example.demolistnew.net.HttpResultTransformer;
import com.example.demolistnew.net.NetClient;
import com.example.demolistnew.net.ProgressSubscriber;
import com.example.demolistnew.util.ToastUtil;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etPhone;
    private EditText etPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        etPhone = (EditText) findViewById(R.id.et_phone);
        etPassword = (EditText) findViewById(R.id.et_password);
        btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                submit();
                break;
        }
    }

    private void submit() {

        String phone = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            ToastUtil.showToast("手机号不能为空");
            return;
        }

        String password = etPassword.getText().toString().trim();
        if (TextUtils.isEmpty(password)) {
            ToastUtil.showToast("密码不能为空");
            return;
        }
        NetClient.getInstance()
                .createUserApi()
                .postLogin(phone, password)
                .compose(new HttpResultTransformer<UserBean>())
                .subscribe(new ProgressSubscriber<UserBean>() {
                    @Override
                    public void onNext(UserBean userBean) {

                    }
                });

    }
}
