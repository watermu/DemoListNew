package com.example.demolistnew.mympv;

import com.example.demolistnew.entity.BannerBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 契约类
 * Created by Administrator on 2017\10\11 0011.
 */

public interface BannerContract {
    /**
     * 轮播图view接口
     */
    interface IBannerView {
        void showEmpty();

        void showBanner(List<BannerBean> bannerBeanList);
    }

    /**
     * 轮播图Presenter接口
     */
    interface IBannerPresenter {

        public void attachView(BannerContract.IBannerView bannerView);

        public void detachView();

        void showBanner();
    }

    /**
     * 轮播图Model接口
     */
    interface IBannerModel {
        void getBanner(BannerResultListener listener);
    }

    interface BannerResultListener {
        void result(ArrayList<BannerBean> bannerBeanList);
    }
}
