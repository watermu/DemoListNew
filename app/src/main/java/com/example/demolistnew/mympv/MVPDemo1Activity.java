package com.example.demolistnew.mympv;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.demolistnew.R;
import com.example.demolistnew.entity.BannerBean;
import com.example.demolistnew.mympv.adapter.BannerViewPagerAdapter;
import com.example.demolistnew.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

public class MVPDemo1Activity extends AppCompatActivity implements BannerContract.IBannerView, View.OnClickListener {


    private ViewPager vpBanner;
    private BannerContract.IBannerPresenter mPresenter;
    private ArrayList<View> mViews;
    private BannerViewPagerAdapter mAdapter;
    private Button btnShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvpdemo1);
        btnShow = (Button) findViewById(R.id.btn_show);

        btnShow.setOnClickListener(this);

        vpBanner = (ViewPager) findViewById(R.id.vp_banner);
        mViews = new ArrayList<>();
        mAdapter = new BannerViewPagerAdapter(mViews);
        vpBanner.setAdapter(mAdapter);

        mPresenter = new BannerPresenterImpl();
        mPresenter.attachView(this);


    }


    @Override
    public void showEmpty() {
        ToastUtil.showToast("空页面");
    }

    @Override
    public void showBanner(List<BannerBean> bannerBeanList) {
        ArrayList<View> views = new ArrayList<>();

        for (BannerBean bean : bannerBeanList) {
            String url = bean.cover;

            View view = LayoutInflater.from(this).inflate(R.layout.item_image, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.iv_price);
            Glide.with(this)
                    .load(url)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(imageView);
            views.add(view);
        }

        mViews.addAll(views);
        mAdapter.notifyDataSetChanged();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_show:
                mPresenter.showBanner();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mViews=null;
        mAdapter=null;
    }
}
