package com.example.demolistnew.mympv;

import com.example.demolistnew.entity.BannerBean;
import com.example.demolistnew.net.HttpResultTransformer;
import com.example.demolistnew.net.NetClient;
import com.example.demolistnew.net.ProgressSubscriber;

import java.util.ArrayList;

/**
 * Author Administrator
 * Create date 2017\10\12 16:05
 * Version 1.0
 * Describe
 */

public class BannerModelImpl implements BannerContract.IBannerModel {

    @Override
    public void getBanner(final BannerContract.BannerResultListener listener) {
        NetClient.getInstance()
                .createHomeApi()
                .getBanner()
                .compose(new HttpResultTransformer<ArrayList<BannerBean>>())
                .subscribe(new ProgressSubscriber<ArrayList<BannerBean>>() {
                    @Override
                    public void onNext(ArrayList<BannerBean> bannerBeen) {
                        if (listener != null) {
                            listener.result(bannerBeen);
                        }
                    }
                });
    }
}
