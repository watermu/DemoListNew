package com.example.demolistnew.mympv.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Author Administrator
 * Create date 2017\10\14 10:56
 * Version 1.0
 * Describe
 */

public class BannerViewPagerAdapter extends PagerAdapter {
    private ArrayList<View> mViews;

    public BannerViewPagerAdapter(ArrayList<View> views) {
        mViews = views;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(mViews.get(position));
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(mViews.get(position));
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mViews == null ? 0 : mViews.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
