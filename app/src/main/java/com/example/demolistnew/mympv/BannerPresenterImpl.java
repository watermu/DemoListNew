package com.example.demolistnew.mympv;

import com.example.demolistnew.entity.BannerBean;

import java.lang.ref.SoftReference;
import java.util.ArrayList;

/**
 * Author Administrator
 * Create date 2017\10\12 16:02
 * Version 1.0
 * Describe 轮播图presenter实现类
 */

public class BannerPresenterImpl implements BannerContract.IBannerPresenter, BannerContract.BannerResultListener {
    private BannerContract.IBannerView mBannerView;
    private final BannerContract.IBannerModel mBannerModel;

    private SoftReference<BannerContract.IBannerView> mReference;

    public BannerPresenterImpl() {
        mBannerModel = new BannerModelImpl();
        mReference = new SoftReference<BannerContract.IBannerView>(mBannerView);
    }

    @Override
    public void showBanner() {
        mBannerModel.getBanner(this);
    }

    @Override
    public void result(ArrayList<BannerBean> bannerBeanList) {
        if (bannerBeanList.size() == 0) {
            mBannerView.showEmpty();
        } else {
            mBannerView.showBanner(bannerBeanList);
        }
    }

    @Override
    public void attachView(BannerContract.IBannerView bannerView) {
        mBannerView = bannerView;
    }

    @Override
    public void detachView() {
        if (mReference != null && mReference.get() != null) {
            mReference.clear();
            mReference = null;
        }
    }
}
