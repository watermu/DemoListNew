package com.example.demolistnew.entity;

/**
 * author Administrator
 * create date 2017\10\12 15:39
 * version 1.0
 * describe 轮播图
 */

public class BannerBean {

    /**
     * name : 发顺丰发安抚是的啊
     * type : 1
     * url : 36
     * cover : https://img2.yyougo.com/9ce12ebcd1895df4a693.jpeg
     */

    public String name;
    public String type;
    public String url;
    public String cover;
}
