package com.example.demolistnew.entity;

/**
 * Created by Administrator on 2017\10\12 0012.
 */

public class UserBean {

    /**
     * audit_guide_no : 1
     * avatar : https://img2.yyougo.com/f61ae545b8836c714505.jpeg
     * balance : 0.00
     * city_version : 1.1
     * id : 42
     * is_guide : 1
     * mobile : 18135705969
     * month_order_amount : 0
     * nick_name : 呵呵
     * pending_amount : 0.00
     * qrcode : https://img2.yyougo.com/qr_guide_42_20170927162802.png
     * rank : 1071
     * service_tel : 400-9158-971
     * state : 0
     * state_cn : 待上传身份认证
     * store_cover :
     * taid : 0
     * today_order_num : 0
     * today_user_num : 0
     * today_wx_subscribe_num : 0
     * token : 54c0a1953e7269b3a2ca2faaef3346ee
     * total_amount : 0.00
     * total_order_amount : 0
     * total_order_num : 0
     * total_user_num : 0
     * total_wx_subscribe_num : 0
     * uid : 71
     * wx_qrcode : https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=gQFW8DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyY0dEb2tmenJjNGsxMDAwMHcwM1UAAgQSYctZAwQAAAAA
     */

    public String audit_guide_no;
    public String avatar;
    public String balance;
    public String city_version;
    public String id;
    public String is_guide;
    public String mobile;
    public String month_order_amount;
    public String nick_name;
    public String pending_amount;
    public String qrcode;
    public String rank;
    public String service_tel;
    public String state;
    public String state_cn;
    public String store_cover;
    public String taid;
    public String today_order_num;
    public String today_user_num;
    public String today_wx_subscribe_num;
    public String token;
    public String total_amount;
    public String total_order_amount;
    public String total_order_num;
    public String total_user_num;
    public String total_wx_subscribe_num;
    public String uid;
    public String wx_qrcode;
}
