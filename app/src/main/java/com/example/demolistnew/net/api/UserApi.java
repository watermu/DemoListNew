package com.example.demolistnew.net.api;

import com.example.demolistnew.entity.UserBean;
import com.example.demolistnew.net.HttpResult;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Administrator on 2017\9\26 0026.
 */

public interface UserApi {
    //    /**
//     * 游客导游合并版 注册
//     * @param phone
//     * @param password
//     * @param recode
//     * @return
//     */
//    @FormUrlEncoded
//    @POST("/v1/user/newregister")
//    Observable<HttpResult<String>> postRegister(@Field("mobile") String phone, @Field("password") String password, @Field("recode") String recode);

    /**
     * 用户登录
     *
     * @param phone    手机号
     * @param password 用户密码
     */
    @FormUrlEncoded
    @POST("/v1/user/login")
    Observable<HttpResult<UserBean>> postLogin(@Field("mobile") String phone, @Field("password") String password);

    @FormUrlEncoded
    @POST("/v1/user/login")
    Observable<String> postStringLogin(@Field("mobile") String phone, @Field("password") String password);
}
