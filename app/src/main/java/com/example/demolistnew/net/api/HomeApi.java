package com.example.demolistnew.net.api;

import com.example.demolistnew.entity.BannerBean;
import com.example.demolistnew.net.HttpResult;

import java.util.ArrayList;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Author Administrator
 * Create date 2017\10\12 15:49
 * Version 1.0
 * Describe
 */

public interface HomeApi {
    @GET("/v1/conf/banner")
    Observable<HttpResult<ArrayList<BannerBean>>> getBanner();
}
