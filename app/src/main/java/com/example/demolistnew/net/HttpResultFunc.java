package com.example.demolistnew.net;


import android.content.Intent;

import com.example.demolistnew.mympv.LoginActivity;
import com.example.demolistnew.net.exception.ApiException;
import com.example.demolistnew.util.UiUtil;

import rx.functions.Func1;

/**
 * Created by Administrator on 2017\8\17 0017.
 */

public class HttpResultFunc<T> implements Func1<HttpResult<T>, T> {
    /*
        const RET_SUCCESS = '0';        成功
        const RET_FAIL = '-1';          -1是业务类型错误
        const RET_USER_FAIL = '-2';     -2用户不存在
        const RET_SING_ERROR = '-3';    -3是签名错误

     */

    @Override
    public T call(HttpResult<T> tHttpResult) {
        if ("0".equals(tHttpResult.ret)) {
            return tHttpResult.data;
        } else {
            if ("-2".equals(tHttpResult.ret) || "-3".equals(tHttpResult.ret)) {
                //使用非activity上下文加新Task标记
                Intent intent = new Intent(UiUtil.getAppContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                UiUtil.getAppContext().startActivity(intent);
            }
            throw new ApiException(tHttpResult.msg);
        }
    }
}
