package com.example.demolistnew.net;


import com.example.demolistnew.net.api.HomeApi;
import com.example.demolistnew.net.api.StoreApi;
import com.example.demolistnew.net.api.UserApi;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Administrator on 2017\8\17 0017.
 */

public class NetClient {
    private static NetClient sInstance;
    private static Retrofit sRetrofit;
    public static String BaseUrl = "http://devapi.yyougo.com/";

    private static OkHttpClient getNewClient() {
        LogInterceptor logging = new LogInterceptor();
        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
    }

    private NetClient() {
        OkHttpClient client = getNewClient();
        sRetrofit = new Retrofit.Builder()
                .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(BaseUrl)
                .build();
    }


    public static NetClient getInstance() {
        if (sInstance == null) {
            synchronized (NetClient.class) {
                if (sInstance == null) {
                    sInstance = new NetClient();
                }
            }
        }
        return sInstance;
    }

    /**
     * 创建api
     *
     * @param T
     * @param <T>
     * @return
     */
    public <T> T createApi(Class<T> T) {
        return sRetrofit.create(T);
    }

    /**
     * 创建用户模块api
     *
     * @return
     */
    public UserApi createUserApi() {
        return sRetrofit.create(UserApi.class);
    }

    /**
     * 创建store模块api
     *
     * @return
     */
    public StoreApi createStoreApi() {
        return sRetrofit.create(StoreApi.class);
    }

    /**
     * 创建主页Api
     * @return
     */
    public HomeApi createHomeApi() {
        return sRetrofit.create(HomeApi.class);
    }
}
