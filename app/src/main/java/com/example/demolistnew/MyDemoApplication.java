package com.example.demolistnew;

import android.app.Application;
import android.content.Context;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

/**
 * Created by Administrator on 2017\9\30 0030.
 */

public class MyDemoApplication extends Application {

    private static Context mContext;
    private RefWatcher mWatcher;

    public static Context getGlobalContext(){
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mWatcher = LeakCanary.install(this);
    }
}
