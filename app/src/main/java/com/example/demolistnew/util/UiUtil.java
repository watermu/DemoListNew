package com.example.demolistnew.util;

import android.content.Context;

import com.example.demolistnew.MyDemoApplication;

/**
 * Created by Administrator on 2017\10\11 0011.
 */

public class UiUtil {
    /**
     * 获取全局上下文
     * @return
     */
    public static Context getAppContext() {
        return MyDemoApplication.getGlobalContext();
    }
}
