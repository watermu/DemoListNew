package com.example.demolistnew.util;

import android.content.Context;
import android.widget.Toast;

import com.example.demolistnew.MyDemoApplication;

/**
 * Created by Administrator on 2017\9\30 0030.
 */

public class ToastUtil {

    private static Toast sToast;

    public static void showToast(String content) {
        if (sToast == null) {
            sToast = Toast.makeText(MyDemoApplication.getGlobalContext(), content, Toast.LENGTH_SHORT);
        } else {
            sToast.setText(content);
        }
        sToast.show();
        sToast=null;
    }

    public static void showToast(Context context,String msg){
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);

        toast.show();
    }
}
